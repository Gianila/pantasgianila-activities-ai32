<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BorrowedBook extends Model
{
    use HasFactory;
    protected $table = 'borrowed_books';
    protected $fillable = ['patron_id', 'copies', 'book_id'];

    public function Book(){
        return $this->belongsTo(Book::class, 'book_id', 'id');
    }
    public function Patron(){
        return $this->belongsTo(Patron::class, 'patron_id', 'id');
    }
}
