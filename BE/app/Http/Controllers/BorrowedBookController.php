<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Request\BorrowedRequest;
use App\Http\Models\BorrowedBook;
use App\Http\Models\ReturnedBook;
use App\Http\Models\Book;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrowesbook = BorrowedBook::all();
        return response()->json
        (["message" => "List of Borrowed Books",
        "data" => $borrowesbook]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowedBookRequest $request)
    {
        $borrowesbook = new BorrowedBook();
        $borrowesbook->copies = $request->copies;
        $borrowesbook->book_id = $request->book_id;
        $borrowesbook->patron_id = $request->patron_id;
        $books = Book::find($borrowesbook->book_id);
        $ibanhinCB = $books->copies - $borrowesbook->copies;

        $validated = $request->validated();
        $borrowesbook->save();
        $books->update(['copies' => $ibanhinCB]);
        return response()->json(
            ["message" => "Successfully",
            "data" => $borrowesbook, $books]
        );

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowesbook = BorrowedBook::find($id);
        return response()->json($borrowesbook);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $borrowesbook = BorrowedBook::find($id);
        $returnesbook = new ReturnedBook();
        $returnedbook->copies = $request->copies;
        $returnedbook->book_id = $request->book_id;
        $returnedbook->patron_id = $request->patron_id;
        $books = Book::find($returnedbook->book_id);
        $addingaCB = $books->copies + $returnedbook->copies;
        $returnedbook->save();
        $borrowesbooks->delete();
        $books->update(['copies' => $addingaCB]);
        return response()->json(["message" => "Successfully",
        "data" => $borrowesbook, $books, $returnedbook]);
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
