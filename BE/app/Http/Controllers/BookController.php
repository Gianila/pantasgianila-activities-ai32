<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Http\Requests\BookRequest;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Books = Book::all();
        return response()->json([
            "message"=>"List of Books",
            "data" => $Books]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $Books = new Book();

        $Books->name = $request->name;
        $Books->author = $request->author;
        $Books->copies = $request->copies;
        $Books->category_id = $request->category_id;
        $validated = $request->validated();
        $Books->save();
        return response()->json($Books);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Books = Book::find($id);
        return response()->json($Books);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Books = Book::find($id);
        $Books->name = $request->name;
        $Books->author = $request->author;
        $Books->copies = $request->copies;
        $Books->category_id = $request->category_id;
        $Books->update();
        return response()->json($Books);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Books = Book::find($id);
        $Books->delete();
        return response()->json($Books);
    }
}
