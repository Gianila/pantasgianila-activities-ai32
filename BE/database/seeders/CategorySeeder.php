<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use App\Models\Category;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
    $mul_rows= [
    [ 'category' => 'Action'],
    [ 'category' => 'Historical Fiction'],
    [ 'category' => 'Non-Fiction'],
    [ 'category' => 'Comic Book']
];

    foreach ($mul_rows as $rows) {
    Category::create($rows);
        }
    }
}
