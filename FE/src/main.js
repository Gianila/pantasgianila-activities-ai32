import Vue from "vue";
import router from "./routes/router";
import store from "./store/store";
import App from "../src/App.vue";
import Toast from "vue-toastification";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true,
});

Vue.use(VueFilterDateFormat);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
import "vue-toastification/dist/index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";

Vue.config.productionTip = false;



new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");

