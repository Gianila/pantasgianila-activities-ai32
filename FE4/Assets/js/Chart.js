var ctx = document.getElementById("barChart").getContext("2d");
var barChart = new Chart(ctx, {
  type: "bar",
  data: {
labels:['January', 'February', 'March', 'April', 'May', 'June', 'July', 'Aug'],
      datasets:[{
        label:'Returned',
        data:[16, 12, 36, 20, 28, 12, 4, 59,],
        backgroundColor:[   
        	'#5094AF',
		'#5094AF',
		'#5094AF',
		'#5094AF',
		'#5094AF',
		'#5094AF',
		'#5094AF',
		'#5094AF',
        ],
      }, 
      
      {
          label:'Borrowed',
          data:[26, 31, 19, 30, 7, 26, 8, 42,], 
        backgroundColor:[
 		'#00212E',
		'#00212E',
		'#00212E',
		'#00212E', 
		'#00212E',
		'#00212E',
		'#00212E',
		'#00212E',
            
        ],
      }],

    },
 
    options:{
   scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  
    }
  });

var pie = document.getElementById("pieChart").getContext("2d");
var pieChart = new Chart(pie, {
  type: "doughnut",
  data: {
    datasets: [
      {
        data: [5, 11, 24, 15],
        backgroundColor: [
          "#4E7A8B",
          "#05384D",
          "#6AC8ED",
          "#FCFF62",
        ],
      },
    ],

    labels: ["Action", "Historical Fiction", "Non-Fiction", "Comic Book"],
  },
  options: {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});
